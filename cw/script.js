const data = [{ "baseCurrency": "UAH", "currency": "EUR", "saleRateNB": 40.8163, "purchaseRateNB": 39.26 },
{ "baseCurrency": "UAH", "currency": "USD", "saleRateNB": 37.4532, "purchaseRateNB": 36.5686 },
{ "baseCurrency": "UAH", "currency": "GBP", "saleRateNB": 46.9484, "purchaseRateNB": 45.2 }];

const myDiv = document.querySelector("#root");

for(i = 0; i <= data.length; i++) {
    const newDiv = document.createElement("p");
    if (data[i].saleRate != undefined) {
    newdiv.innerHTML = `Курс ${data[i].currency}  до гривні НБУ: ${data[i].saleRateNB} / ${data[i].purchaseRateNB}, ринок: ${data[i].saleRate} / ${data[i].purchaseRate}`
    } else {
        newDiv.innerHTML = `Курс ${data[i].currency}  до гривні НБУ: ${data[i].saleRateNB} / ${data[i].purchaseRateNB}`
    }
    myDiv.appendChild(newDiv);   
}